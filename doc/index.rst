.. pysolarcalc documentation master file, created by
   sphinx-quickstart on Mon Jul 12 15:29:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pysolarcalc's documentation!
=======================================
A python port of SolarCalc by Kurt Spokas.

Follows formulae largely from
`Spokas and Forcella (2006) <https://pubag.nal.usda.gov/catalog/1910>`_ and
`this NREL tech report <https://rredc.nrel.gov/solar/pubs/spectral/model/section3.html>`_.

.. image:: https://gitlab.com/appf-anu/pysolarcalc/badges/master/pipeline.svg  
   :target: https://gitlab.com/appf-anu/pysolarcalc/-/commits/master
   :alt: pipeline status

   
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   installation
   usage
   Adding new lights <addinglights>
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
