#!/usr/bin/env python3

# Usage: csv2solarcalc <input csv file>
# See format from csv2solarcalc_inputs.csv

import sys
import csv
import calendar

csv_file = sys.argv[1] 

additional_args = "-i 720"

with open(csv_file, 'r') as file:
    reader = csv.reader(file)
    first_row = True
    for row in reader:
        if first_row is True:
            first_row = False
            continue
        # header format: Site_name,Lat,Lon,Elevation_m,Year,Month
        filename = row[0].replace(" ", "-") + ".csv"
        location = row[1] + ' ' + row[2] + ' ' + row[3]
        year = row[4]
        month = row[5]
        if month == "all":
            startdate = year + '-01-01'
            enddate = year + '-12-31'
        else:
            yearmonth = year + '-' + month + '-'
            startdate = yearmonth + '01'
            enddate = yearmonth + str(calendar.monthrange(int(year), int(month))[1])

        command = "pysolarcalc -l just_solar_calibration.csv --start " + \
            startdate + " --end " + enddate + " -p " + location + " " + additional_args + " -o " + filename
        
        print(command)
