#!/usr/bin/env python3

import sys
import csv

# Takes a SpectraPen SP100 output file.
# (exported as Spectrum Irradiance (uW/cm2/nm only))

# use `spectrapen_converter.py <edited spectrapen file in> <csv out>`
spectrapen_raw = sys.argv[1]
spectra_out = sys.argv[2]

csv_final = []

with open(spectrapen_raw) as csv_orig:
    csvreader = csv.reader(csv_orig)
    # skip first line, which usually just says "irradiance"
    header = next(csvreader)
    if ('[nm]' not in header[0]):  # some files count lines weird, hacky fix
        header = next(csvreader)
    header[0] = "wavelength"  # swap "[nm]" for "wavelength"
    csv_final.append(header)  # add the headers to export
    for line in csvreader:
        processed_line = [line[0], ]
        readings = line[1:]
        for reading in readings:
            final_num = float(reading) / 100  # convert from uW/cm^2 to W/m^2
            processed_line.append(final_num)
        csv_final.append(processed_line)

# actually export file
with open(spectra_out, 'w') as csv_out:
    export = csv.writer(csv_out)
    export.writerows(csv_final)
