from openpyxl import Workbook
import datetime
import io
import numpy as np
import os


class Output(object):
    def __init__(self, file, headers, workbook=None, sheet_title='timepoints', ):
        self.file = file
        self.headers = headers
        self.data = []
        self.is_excel = os.path.splitext(self.file.name)[-1] == '.xlsx'
        if not self.is_excel:
            return

        self.save = False
        if workbook is not None:
            self.workbook = workbook
        else:
            self.workbook = Workbook(write_only=True)
            self.save = True  # keep track of when its our workbook and isnt just passwd to us.
        self.ws = self.workbook.create_sheet(title=sheet_title)
        # datetime has 19 characters
        self.ws.column_dimensions['A'].width = 19
        self.ws.column_dimensions['B'].width = 19
        self.ws.append(self.headers)

    def add_data(self, *line):
        self.data.append(line)
        if self.is_excel:
            self.ws.append(line)

    def output(self, **kwargs):
        if self.is_excel:
            if self.save:
                self.workbook.save(filename=self.file)
            return
        self.output_csv(**kwargs)

    def output_csv(self, separator=','):
        def f2(x):
            return "{:0.2f}".format(np.round(x, 2))

        sio = io.StringIO()
        print(*self.headers, sep=separator, file=sio)
        for l in self.data:
            line = list(l)
            for i, x in enumerate(line):
                if isinstance(x, datetime.datetime):
                    line[i] = x.isoformat(timespec='seconds')
                elif isinstance(x, str):
                    line[i] = x
                else:
                    line[i] = f2(x)
            print(*line, sep=separator, file=sio)
        if 'b' in self.file.mode:
            self.file.write(sio.getvalue().encode('utf-8'))
        else:
            self.file.write(sio.getvalue())
