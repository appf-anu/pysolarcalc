# Copyright (c) 2018-2019 Kevin Murray <foss@kdmurray.id.au>
# Copyright (c) 2018 Gareth Dunstone

# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, you can
# obtain one at http://mozilla.org/MPL/2.0/.


import numpy as np
from tqdm import tqdm

import argparse
import datetime
import os
from dateutil import parser

from solarcalc.light_sim import LightSim, daterange
from solarcalc.spectopt import Light, Spectrum, BandCost, SimpleCost
from solarcalc.file_interface import Output


def valid_date(s):
    try:
        if isinstance(s, datetime.datetime) or isinstance(s, datetime.date):
            return s
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date in Y-m-d form: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def main():
    p = argparse.ArgumentParser()
    p.add_argument("-s", "--start", type=valid_date, default="2000-01-01", metavar="DATE",
                   help="Simulation start date")
    p.add_argument("-e", "--end", type=valid_date, default="2000-12-31", metavar="DATE",
                   help="Simulation end date")
    p.add_argument("-i", "--interval", type=int, default=10, metavar="MINUTES",
                   help="Simulation interval (minutes)")
    p.add_argument("-S", "--chamber-start", type=valid_date, default="2020-01-01", metavar="DATE",
                   help="Real date on which conditions will be run in a chamber")
    p.add_argument("-r", "--scale-factor", type=float, default=0.5,
                   help="Proportion of expected total irradiance to replicate in chambers. Controls for the fact that chambers can't get to full intensity. Set to approx max(chamber) / max(sunshine)")
    p.add_argument("-c", "--channel-headers", action='store_true',
                   help="Use channel-# format for channel header number")
    p.add_argument("--chamber-spectrum", type=argparse.FileType('wb'), metavar="FILE",
                   help="Write actual spectrum experienced in the growth chamber to FILE")
    p.add_argument("--solar-spectrum", type=argparse.FileType('wb'), metavar="FILE",
                   help="Write simulated solar spectrum to FILE")
    p.add_argument("--spectra-as-watts", action="store_true",
                   help="Write chamber/simulated spectra in units of watts per m2 per s (default is in uE)")
    p.add_argument("-p", "--place", type=float, required=True, nargs=3, metavar=("LAT", "LON", "ELEVATION"),
                   help="Location of simulation as lat, long, elevation in decimal degrees/meters")
    p.add_argument("-l", "--light", type=str, required=True,
                   help="Light definition file")
    p.add_argument("-o", "--output", type=argparse.FileType('wb'), default='-',
                   help="TSV or XLSX output file")
    args = p.parse_args()

    lat, lon, elev = args.place
    wavelengths = np.arange(350, 800, 5, dtype=float)
    sim = LightSim(args.start, args.end, lat, lon, elev, wavelengths=wavelengths)

    light = Light(args.light)

    headers = ["datetime", "datetime-sim", "temperature", "humidity", "total_solar"]
    if args.channel_headers:
        headers += ["channel-{}".format(i + 1) for i in range(len(light.channels))]
    else:
        headers += [*light.channels]

    output = Output(args.output, headers)
    if output.is_excel:
        if args.chamber_spectrum is None:
            args.chamber_spectrum = args.output
        if args.solar_spectrum is None:
            args.solar_spectrum = args.output

    if args.chamber_spectrum is not None:
        wltxt = [f"{l}nm" for l in wavelengths]
        if args.chamber_spectrum.name == args.output.name:
            chamber_spectrum_output = Output(args.chamber_spectrum, ["datetime", "datetime-sim", "PAR", *wltxt],
                                             workbook=output.workbook, sheet_title="chamber-spectrum")
        else:
            chamber_spectrum_output = Output(args.chamber_spectrum, ["datetime", "datetime-sim", "PAR", *wltxt],
                                             sheet_title="chamber-spectrum")

    if args.solar_spectrum is not None:
        wltxt = [f"{l}nm" for l in wavelengths]
        if args.solar_spectrum.name == args.output.name:
            solar_spectrum_output = Output(args.solar_spectrum, ["datetime", "datetime-sim", "PAR", *wltxt],
                                           workbook=output.workbook, sheet_title="solar-spectrum")
        else:
            solar_spectrum_output = Output(args.solar_spectrum, ["datetime", "datetime-sim", "PAR", *wltxt],
                                           sheet_title="solar-spectrum")

    date_offset = args.chamber_start - args.start
    total = (args.end - args.start).total_seconds() / (60 * args.interval)
    for d in tqdm(daterange(args.start, args.end, minutes=args.interval), total=total):
        doyf = d.timetuple().tm_yday + d.minute / 1440.0 + d.hour / 24.0
        theory = sim.combined_spline(doyf)
        temp, rh, totsrad, *spectrum = theory
        spectrum = Spectrum(wavelengths=wavelengths,
                            values=np.array(spectrum) / 1000 * args.scale_factor,
                            watts=True)
        opt = light.optimise_settings(spectrum, watts=False)
        reald = d + date_offset

        # def f2(x): return "{:0.2f}".format(np.round(x, 2))
        # realds = reald.isoformat(timespec='seconds')
        # modelds = d.isoformat(timespec='seconds')
        output.add_data(reald, d, temp, rh, totsrad, *opt)
        # print(realds, modelds, f2(temp), f2(rh), f2(totsrad), *map(f2, opt), sep="\t", file=args.output)
        if args.chamber_spectrum is not None:
            light_output = light.light_output(opt)
            if args.spectra_as_watts:
                out = light_output.watts().interpolated()(wavelengths)
            else:
                out = light_output.photons().interpolated()(wavelengths) * 1e9  # 1e9 is for uE
            chamber_spectrum_output.add_data(reald, d, *out)
            # print(realds, modelds, f2(output.par()), *map(f2, out), sep="\t", file=args.chamber_spectrum)
        if args.solar_spectrum is not None:
            if args.spectra_as_watts:
                out = spectrum.watts().interpolated()(wavelengths)
            else:
                out = spectrum.photons().interpolated()(wavelengths) * 1e9  # 1e9 is for uE
            solar_spectrum_output.add_data(reald, d, *out)
            # print(realds, modelds, f2(spectrum.par()), *map(f2, out), sep="\t", file=args.solar_spectrum)

    if args.chamber_spectrum is not None:
        chamber_spectrum_output.output(separator='\t')
    if args.solar_spectrum is not None:
        solar_spectrum_output.output(separator='\t')

    output.output(separator='\t')


def fit():
    p = argparse.ArgumentParser()
    p.add_argument("-r", "--scale-factor", type=float, default=0.5,
                   help="Proportion of expected total irradiance to replicate in chambers. Controls for the fact that chambers can't get to full intensity. Set to approx max(chamber) / max(sunshine)")
    p.add_argument("-c", "--channel-headers", action='store_true',
                   help="Use channel-# format for channel header number")
    p.add_argument("-t", "--target-spectrum", type=argparse.FileType('r'), metavar="FILE", required=True,
                   help="Read desired spectrum from CSV file.")
    p.add_argument("-l", "--light", type=str, required=True,
                   help="Light definition file")
    p.add_argument("-o", "--output", type=argparse.FileType('w'), default='-',
                   help="CSV Output file")
    p.add_argument("--normalize", type=str, default="none",
                   help="Normalization mode - none (no normalisation)/percent (sum to 1)/maxonechannel (highest light channel is 1)")
    args = p.parse_args()

    light = Light(args.light)
    target = Spectrum.read(args.target_spectrum)
    headers = ["total_light", ]
    if args.channel_headers:
        headers += ["channel-{}".format(i + 1) for i in range(len(light.channels))]
    else:
        headers += [*light.channels]

    print(target.values)
    # print(light)
    opt = light.optimise_settings(target, watts=True)
    print(*headers[1:], sep=",", file=args.output)
    if args.normalize == "none":
        print(*opt, sep=",", file=args.output)
    elif args.normalize == "percent":
        norm_factor = sum(opt)
        normed_opt = [i / norm_factor for i in opt]
        print(*normed_opt, sep=",", file=args.output)
    elif args.normalize == "maxonechannel":
        norm_factor = max(opt)
        normed_opt = [i / norm_factor for i in opt]
        print(*normed_opt, sep=",", file=args.output)


if __name__ == "__main__":
    main()
